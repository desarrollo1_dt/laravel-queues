<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>Soap - Service</title>

    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/examples/cover/cover.css" rel="stylesheet">

  
    <script src="https://getbootstrap.com/docs/3.3/assets/js/ie-emulation-modes-warning.js"></script>

</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">
          
            <section class="inner cover">
               
              
        
            <section>
                <div class="panel panel-default">
                    <div class="panel-heading">Respuesta WSDL</div>
                    <div class="panel-body" style="color: #000 !important;">
                        <ul style="list-style-type: none">
                            <li>{{ $response->countryCode }}</li>
                            <li>{{ $response->vatNumber }}</li>
                            <li>{{ $response->requestDate }}</li>
                            <li>{{ $response->valid }}</li>
                            <li>{{ $response->name }}</li>
                            <li>{{ $response->address }}</li>
                        </ul>
                    </div>
                </div>

                <div>
                    <form action="" method="get">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Código del país</label>
                            <input type="text" class="form-control" name="countryCode" placeholder="DK" value="DK">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Número de IVA</label>
                            <input type="text" class="form-control" name="vatNumber" placeholder="47458714" value="47458714">
                        </div>
                        <button type="submit" class="btn btn-default">Obtener</button>
                    </form>
                </div>
            </section>
        </section>
        </div>

    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
<script src="https://getbootstrap.com/docs/3.3/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
