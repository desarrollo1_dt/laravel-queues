<?php
Route::get('biene', 'SoapController@BienesServicios');


Route::get('mostrarDatos', function () {
    try {

        $opts = array(
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        );
        $context = stream_context_create($opts);

        $soapClientOptions = array(
            'stream_context' => $context,
            'cache_wsdl' => WSDL_CACHE_NONE
        );

        $wsdlUrl = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

        $client = new SoapClient($wsdlUrl, $soapClientOptions);

        $checkVatParameters = array(
            'countryCode' => 'DK',
            'vatNumber' => '47458714'
        );

        $result = $client->checkVat($checkVatParameters);
        dd($result);
        // print_r($result);
    }
    catch(\Exception $e) {
        echo $e->getMessage();
    }

});

Route::get('ver', function () {

    // parametros para conectar con el web service
    
 $opts = array(
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        );
    

    // otro metodo
    // $opts = array(
    //     'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
    // );
    

    $params = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) );



    $url = "http://sandbox.coordinadora.com/ags/1.4/server.php?wsdl";

    try{

        // instanciar la libreria soapClient 
        // 
        $client = new SoapClient($url,$params);
        
        dd($client->__getTypes());
        
    }
    catch(SoapFault $fault) {
        echo '<br>'.$fault;
    }

});